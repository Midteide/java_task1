import java.util.Random;

public class Task1 {

public static final String ANSI_RESET = "\u001B[0m";
public static final String ANSI_BLACK = "\u001B[30m";
public static final String ANSI_RED = "\u001B[31m";
public static final String ANSI_GREEN = "\u001B[32m";
public static final String ANSI_YELLOW = "\u001B[33m";
public static final String ANSI_BLUE = "\u001B[34m";
public static final String ANSI_PURPLE = "\u001B[35m";
public static final String ANSI_CYAN = "\u001B[36m";
public static final String ANSI_WHITE = "\u001B[37m";

public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

public static String [][] colours = {{ANSI_BLACK, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN,ANSI_WHITE}, {ANSI_BLACK_BACKGROUND, ANSI_RED_BACKGROUND, ANSI_GREEN_BACKGROUND, ANSI_YELLOW_BACKGROUND, ANSI_BLUE_BACKGROUND, ANSI_PURPLE_BACKGROUND, ANSI_CYAN_BACKGROUND,ANSI_WHITE_BACKGROUND}};

public static String myName = "Alexander";
    public static Random rand = new Random(); 
    public static void main( String[] args ) {
        System.out.println( "Hello Craig!" );
        System.out.printf( "My name is ");
        printInColours(myName);
        System.out.println(".");
    }

    public static void printInColours(String str){
        for (int i=0; i < str.length() ; i++ ) {
            // Obtain a number between [0 - 7]:
            int randNumberText = rand.nextInt(8);
            int randNumberBackground = rand.nextInt(8);

            // Make sure the background and text color are not the same:
            while (randNumberBackground == randNumberText) randNumberBackground = rand.nextInt(8);

            // Print out character
            System.out.printf(colours[1][randNumberBackground] + colours[0][randNumberText] + str.charAt(i) + "" + ANSI_RESET);
        }
    }
}   